<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int $frequency
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['articles'], 'safe'],
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'frequency' => 'Frequency',
            'name' => 'Name',
        ];
    }
   
    public static function findAllByName($name)
    {
        return Category::find()->where('name LIKE :query')
                          ->addParams([':query'=>"%$name%"])
                          ->limit(50)
                          ->all();
    }

    public function getArticles(){
        return $this->hasMany(Article::className(), ['id' => 'article_id'])->viaTable('category_article_assn', ['category_id' => 'id']);
    }
}
