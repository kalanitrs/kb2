<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category_article_assn`.
 */
class m180806_104805_create_category_article_assn_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category_article_assn', [
            'category_id' => $this->integer(),
            'article_id' => $this->integer(),
            'PRIMARY KEY(category_id,article_id)'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category_article_assn');
    }
}
