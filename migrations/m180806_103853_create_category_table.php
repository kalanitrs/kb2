<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m180806_103853_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'frequency' => $this->integer()
        ]);

        $this->insert('category', [
            'name' => 'friends',
            'frequency' => 0
        ]);

        $this->insert('category', [
            'name' => 'food',
            'frequency' => 0
        ]);

        $this->insert('category', [
            'name' => 'animals',
            'frequency' => 0
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
