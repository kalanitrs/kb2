<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180806_102307_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'username' => $this->string()->unique(),
            'auth_key' => $this->string(),
            'password' => $this->string(),
        ]);

        $this->addForeignKey(
            'fk-article-created_by',
            'article',
            'created_by',
            'user',
            'id'
        );

        $this->addForeignKey(
            'fk-article-updated_by',
            'article',
            'updated_by',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-article-created_by',
            'user'
        );
        $this->dropForeignKey(
            'fk-article-updated_by',
            'user'
        );
        $this->dropTable('user');
    }
}
