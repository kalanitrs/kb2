<?php

use yii\db\Migration;

/**
 * Class m180808_110120_init_rbac
 */
class m180808_110120_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $author = $auth->createRole('author');
        $auth->add($author);

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $editor = $auth->createRole('editor');
        $auth->add($editor);
              
        $auth->addChild($admin, $author);
        $auth->addChild($admin, $editor);

        $manageUsers = $auth->createPermission('manageUsers');
        $auth->add($manageUsers);

        $createArticle = $auth->createPermission('createArticle');
        $auth->add($createArticle);

        $updateArticle = $auth->createPermission('updateArticle');
        $auth->add($updateArticle);

        $deleteArticle = $auth->createPermission('deleteArticle');
        $auth->add($deleteArticle);
              
        $updateOwnArticle = $auth->createPermission('updateOwnArticle');

        $rule = new \app\rbac\AuthorRule;
        $auth->add($rule);
              
        $updateOwnArticle->ruleName = $rule->name;                
        $auth->add($updateOwnArticle);                                       
           
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($author, $createArticle);
        $auth->addChild($editor, $updateArticle);
        $auth->addChild($editor, $deleteArticle);
        $auth->addChild($updateOwnArticle, $updateArticle);
        $auth->addChild($author, $updateOwnArticle); 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180806_105310_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180806_105310_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
