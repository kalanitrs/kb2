<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use ogheo\comments\widget\Comments;
 

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description',
            'body:ntext',
            [
                'label' => 'Category',
                'format' => 'html',
                //'value' => Html::a($model->category->name,
                //    ['category/view', 'id' => $model->category->id]),
                'value' => Html::a($model->category->name, ['article/index', 'ArticleSearch[category]' => $model->category->name])
            ],
            [
                'label' => 'Tags',
                'format' => 'html',
                'value' => $tags
            ],
            [
                'label' => 'Created By',
                'format' => 'html',
                'value' => Html::a($model->createdBy->name,
                    ['users/view', 'id' => $model->createdBy->id]),
            ],
            [
                'label' => 'Updated By',
                'format' => 'html',
                'value' => Html::a($model->updatedBy->name,
                    ['users/view', 'id' => $model->updatedBy->id]),
            ],
        ],
    ]) ?>
    <?php echo Comments::widget();?>

</div>
